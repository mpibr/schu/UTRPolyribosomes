# UTRPolyribosomes
3'UTR-End Sequencing on polyribosome fractions

## Layout

samples (x 2 replica):

* input (bound and unbound fraction together)
* 80S (monosome fraction)
* 2and3
* 4and5
* 6and7
* \>7

\+ spike-in controls (ERCC)

The library preparation is done using [MACE](https://www.ncbi.nlm.nih.gov/pubmed/25052703) method. Reads are sequenced on Illumina NextSeq 550.

## Preprocessing

### Spike-ins
|samples|concentration [ng/ul]|total.rna [ng]|rounded.rna [ng]|ERCCs volume [ul]|ERCCs dilution|
|:-----:|:-----------:|:-------:|:---------:|:----------:|:------------:|
|Input|164.5|3290|3500|7|1:100|
|Unbound|13|260|300|6|1:1000|
|80s|21|420|500|10|1:1000|
|2and3|39|780|800|16|1:1000|
|4and5|54|1080|1100|22|1:1000|
|6and7|57|1140|1200|24|1:1000|
|>7|170|3400|3500|7|1:100|

### Normalisation
