function data = readCountTable(fileName)
% READCOUNTTABLE

    fh = fopen(fileName, 'r');
    header = fgetl(fh);
    data.header = regexp(header, '\t', 'split');
    fmt = repmat({'%n'}, length(data.header), 1);
    fmt(1) = {'%s'};
    fmt = sprintf('%s ', fmt{:});
    fmt(end) = [];
    txt = textscan(fh, fmt, 'delimiter', '\t');
    fclose(fh);
    data.label = txt{1};
    data.counts = [txt{2:end}];

end

