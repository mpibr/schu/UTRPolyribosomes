% normaliseErcc
clc
clear variables
close all


%% read count file
data = readCountTable('../tables/countTable_UTRPolysomes_UQNormed_24Nov2017.txt');

%% extract ercc
idxErcc = strncmp('ERCC', data.label, 4);
ercc.ids = data.label(idxErcc);
ercc.counts = data.counts(idxErcc,:);


%% read ercc mix concentration
fr=fopen('../tables/cms_095046.txt','r');
fgetl(fr);
txt = textscan(fr,'%*s %s %*s %n %n %*s %*s','delimiter','\t');
fclose(fr);
mix = txt{2};
[~, idxRef, idxQry] = intersect(txt{1}, ercc.ids);
ercc.mix(idxQry,1) = mix(idxRef);

%% assign weight
ercc.volume = [7, 10, 16, 22, 24, 7];
ercc.dilution = 1./[100, 1000, 1000, 1000, 1000, 100];
ercc.weight = ercc.volume .* ercc.dilution;
ercc.weight = repmat(ercc.weight, 2, 1);
ercc.weight = ercc.weight(:);

%% calculate concentration
ercc.attomoles = ercc.mix * ercc.weight';

%% linear regression
clrmtx = jet(6);
figure('color','w');
i = 1;
pArray = zeros(12, 2);
hold on;
k = 1;
for k = 1 : 12
    X = log2(ercc.counts(:,k) + 1);
    Y = log2(ercc.attomoles(:,k) + 1);
    
    idxFilter = X >= 4;
    plot(X,Y,'.','color',clrmtx(i,:));
    p = polyfit(X(idxFilter),Y(idxFilter),1);
    pArray(k,:) = p;
    xfit = linspace(min(X),max(X),100);
    yfit = polyval(p, xfit);
    plot(xfit, yfit, '-','color',clrmtx(i,:),'linewidth',1.2);
    
    if mod(k,2) == 0
        i = i + 1;
    end
    
end 
hold off;

nfactor = mean(pArray(:,1))./pArray(:,1);
ncounts = bsxfun(@times, data.counts(~idxErcc,:), nfactor') + 1;

%% write out normed counts
fmtout = repmat({'%.6f\t'},size(ncounts,2)+1,1);
fmtout(1) = {'%s\t'};
fmtout(end) = {'%.6f\n'};
fmtout = sprintf('%s',fmtout{:});
mtxout = [data.label(~idxErcc),num2cell(ncounts)]';
hdrout = sprintf('%s\t',data.header{:});
hdrout(end) = [];

fw = fopen('../tables/countTable_UTRPolysomes_ERCCNormed_24Nov2017.txt','w');
fprintf(fw,'%s\n',hdrout);
fprintf(fw,fmtout,mtxout{:});
fclose(fw);

%% plot results
%{
rle = log2(ncounts ./ median(ncounts(:)));
figure('color','w');
hold on;
plot([0.5,12.5],[0,0],'k');
boxplot(rle,'Symbol','','colors',kron(clrmtx,ones(2,1)));
hold off
set(gca,'Box','off',...
        'XTickLabel',data.header(2:end),...
        'XTickLabelRotation',45,...
        'YLim',[-5,8.5]);


c = pca(ncounts,'Algorithm','eig','Centered',true);
i = 1;
figure('color','w');
hold on;
for k = 1 : 12
    
    h(k) = plot(c(k,1),c(k,2),'.','color',clrmtx(i,:),'MarkerSize',20);
    if mod(k,2) == 0
        i = i + 1;
    end
end
hl = legend(h(1:2:end),{'Input','80s','2and3','4and5','6and7','>7'});
set(hl,'Location','NorthWest','EdgeColor','w');

hold off;
%}



