% polyribosomeAnalysis
clc
clear variables
close all

%{
tmp = readCountTable('../tables/countTable_UTRPolysomes_RUVg_22Nov2017.txt');

x = tmp.counts(:,1:2);
y = tmp.counts(:,3:2:end);
z = tmp.counts(:,4:2:end);

m = (y + z)./2;
mr = m ./ sum(m, 2);

w = [1,2.5,4.5,6.5,7.5];

scr = sum(bsxfun(@times, mr, w./mean(w)),2);

atest = {'fwdgroup00444122',...
'fwdgroup00444136',...
'fwdgroup00444148',...
'fwdgroup00444188',...
'fwdgroup00444190',...
'fwdgroup00444192'};

[~,idxRef] = intersect(tmp.label, atest);

N = 10;
idx = kmeans(mr,N);
clrmtx = jet(N);
figure('color','w');
hold on;
for k = 1 : N
    
    plot((1:5),median(mr(idx==k,:)),'color',clrmtx(k,:));
    
end
hold off;

%}
%% read stats file
%stats = readCountTable('../tables/countTable_UTRPolysomes_Stats_21Nov2017.txt');

%% read ercc file
ercc = readCountTable('../tables/countTable_UTRPolysomes_ERCC_21Nov2017.txt');
fr=fopen('../tables/cms_095046.txt','r');
fgetl(fr);
txt = textscan(fr,'%*s %s %*s %n %n %*s %*s','delimiter','\t');
fclose(fr);
ercc.ref=txt{1};
ercc.mix1=txt{2};
ercc.mix2=txt{3};
ercc.volume = [7,10,16,22,24,7];
ercc.volume = repmat(ercc.volume, 2, 1);
ercc.volume = ercc.volume(:)';

ercc.dilution = [100,1000,1000,1000,1000,100];
ercc.dilution = repmat(ercc.dilution, 2, 1);
ercc.dilution = ercc.dilution(:)';
ercc.weight = ercc.volume ./ ercc.dilution;

ercc.mix = repmat(ercc.mix1,1,size(ercc.counts,2));
ercc.conc = bsxfun(@times, ercc.mix, ercc.weight);

[~,idxQry, idxRef] = intersect(ercc.label, ercc.ref);
Y = ercc.conc(idxRef,:);
X = ercc.counts(idxQry,:);




idxFilter = (sum(Y >= 5, 2) >= 2);
X = X(idxFilter,:);
Y = Y(idxFilter,:);

YY = bsxfun(@rdivide, Y, sum(Y));


clrmtx = jet(12);
PP = zeros(12,2);
figure('color','w');
hold on;
for k = 1 : 12
    
    pX = log2(X(:,k));
    pY = log2(Y(:,k));
    p = polyfit(pX,pY,1);
    PP(k,:) = p;
    plot(pX,pY,'.','color',clrmtx(k,:));
    xfit = linspace(min(pX),max(pX),100);
    yfit = polyval(p, xfit);
    plot(xfit,yfit, 'color',clrmtx(k,:));
    
end

xfit = linspace(2,15,100);
yfit = polyval(median(PP),xfit);
plot(xfit, yfit, 'color','k','linewidth',1.2);

hold off;





%% read UTRs file
%{
fh = fopen('countTable_UTRPolysomes_UTRs_21Nov2017.txt', 'r');
header = fgetl(fh);
header = regexp(header, '\t', 'split');
fmt = repmat({'%n'}, length(header), 1);
fmt(1:3) = {'%s'};
fmt = sprintf('%s ', fmt{:});
fmt(end) = [];
txt = textscan(fh, fmt, 'delimiter', '\t');
fclose(fh);
data.passid = txt{1};
data.symbol = txt{2};
data.feature = txt{3};
data.window = txt{4};
data.span = txt{5};
data.reads = txt{6};
data.counts = [txt{7:end}];
%}

